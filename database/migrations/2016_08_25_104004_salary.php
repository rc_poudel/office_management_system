<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Salary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('staff_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id')
                  ->foreign('staff_id')->references('id')->on('staff');
            $table->integer('month_id')
                  ->foreign('month_id')->references('id')->on('months');
            $table->integer('year');
            $table->decimal('salary', 7, 2);    
            $table->decimal('tax_amount', 7, 2);    
            $table->decimal('total_salary', 7, 2);
            $table->dateTime('created_at'); 
            $table->dateTime('updated_at');
            
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staff_salary');
    }
}
