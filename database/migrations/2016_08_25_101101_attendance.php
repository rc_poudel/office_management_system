<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id')
                  ->foreign('staff_id')->references('id')->on('staff');
            $table->integer('card_id');
            $table->dateTime('arrival_time'); 
            $table->dateTime('departure_time');
            $table->enum('status', ['absent','fullDay','halfDay']);
            $table->text('remarks');
            
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attendance');
    }
}
