<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Staff extends Migration
{

    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 32);
            $table->string('last_name', 32);
            $table->string('position', 32);
            $table->integer('mobile');
            $table->string('email', 128)->unique();
            $table->text('address');
            $table->date('date_of_join');
            $table->enum('marital_status', ['married', 'unmarried']);    
            $table->dateTime('created_at'); 
            $table->dateTime('updated_at');
        });
    }

    public function down()
    {
        Schema::drop('staff');
    }
}
