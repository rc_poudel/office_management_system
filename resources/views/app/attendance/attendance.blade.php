    @extends('app.layouts.default')
    @section('content')
        <div class="container">
            <h1>Attendance</h1>
            {{ link_to_route('attendance.create', 'Staff Attendance', null,['class'=>'btn btn-primary']) }}

            <hr>

            <section align="right">
                <h3>Staff Attendance Sheet</h3>
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">Card No.</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Post</th>
                        <th class="text-center">Arrival time</th>
                        <th class="text-center">Departure time</th>
                        <th class="text-center">Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($attendance as $a)
                        <tr>
                            <td>{{ $a->staff->id }}</td>
                            <td>{{ $a->staff->first_name." ".$a->staff->last_name }}</td>
                            <td>{{ $a->staff->position }}</td>
                            <td>{{ $a->arrival_time }}</td>
                            <td>{{ $a->departure_time }}</td>
                            <td>{{ $a->remarks }} </td>
                        </tr>
                @endforeach
                    </tbody>
            </section>


        </div>
    @stop
