@extends('app.layouts.create')
@section('content')

    <div class="container">
        <h1>Staff Attendance</h1>
        {{ link_to_route('attendance.index', 'Attendance sheet', null,['class'=>'btn btn-primary']) }}
        <hr>

        @include('app.includes.errors')

        {!! Form::open(['route'=>'attendance.store']) !!}

        @include('app.attendance.form')
        <button type="submit" name="office_time" id="arrival_time" class="btn btn-info btn-lg" value="arrival_time"><i class="fa fa-calendar-check-o" aria-hidden="true"> Arrival Time</i>
        </button>

        <button type="submit" name="office_time" id="departure_time" class="btn btn-success btn-lg" value="departure_time"><i class="fa fa-calendar-check-o" aria-hidden="true"> Departure Time</i>
        </button>


        {!! Form::close() !!}


    </div>


@stop