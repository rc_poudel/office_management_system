

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 form-padding">
        <label class="col-xs-12 col-sm-12 col-md-8" >Name</label>
        <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
            <select id="staff_id" name="staff_id" class="form-control">
                  @foreach($staff as $s)
                    <option value="{{ $s->id }}">{{ $s->first_name." ".$s->last_name }}</option>

                @endforeach
            </select>
        </div>
    </div>
</div>

