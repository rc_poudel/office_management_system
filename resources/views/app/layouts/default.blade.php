<!DOCTYPE html>
<html lang="en">

<head>
     @include('app.includes.head')
    
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
            @include('app.includes.nav')

        <!-- End Navigation -->

        <!-- Sidebar -->
            @include('app.includes.sidebar')

        <!-- End Sidebar -->
            
        @yield('content')
            

        
    </div>
    <!-- /#wrapper -->

    

</body>

    @include('app.includes.script')


</html>
