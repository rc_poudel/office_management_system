<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Staff Management System</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../public/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../public/css/style.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="../public/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.min.css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
            @include('app.includes.nav')

        <!-- End Navigation -->

        <!-- Sidebar -->
            @include('app.includes.sidebar')

        <!-- End Sidebar -->
            
        @yield('content')
            

        
    </div>
    <!-- /#wrapper -->

    

</body>

    <script src="../public/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../public/js/sb-admin-2.min.js"></script>



</html>
