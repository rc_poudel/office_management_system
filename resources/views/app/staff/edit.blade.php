@extends('app.layouts.edit')
@section('content')

<div class="container">
<h1>Edit</h1>
{{ link_to_route('staff.index', 'View All Staff', null,['class'=>'btn btn-primary']) }}
<hr>

	@include('app.includes.errors')

{!! Form::model($staff, [
    'method' => 'PATCH',
    'route' => ['staff.update', $staff->id]
]) !!}	
	<div align="right">
	    {{ Form::reset('Reset All', ['class' =>'btn btn-warning']) }}
	</div>
    @include('app.staff.form')

    {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}



{!! Form::close() !!}
</div>
@stop