@extends('app.layouts.edit')
@section('content')

<div class="container">

<h1>Staff Information</h1>
    {{ link_to_route('staff.index', 'View All Staff', null,['class'=>'btn btn-primary']) }}
    <hr>

	<div class="table-responsive">

		<table class="table table-bordered">
			<tr>
				<th width="25%">Name</th>
				<td>{{ $staff->first_name." ".$staff->last_name }}</td>
			</tr>

			<tr>
				<th width="25%">Email</th>	
				<td>{{ $staff->email }}</td>
			</tr>

			<tr>
				<th width="25%">Mobile</th>	
				<td>{{ $staff->mobile }}</td>
			</tr>

			<tr>
				<th width="25%">CV</th>	
				<td>{{ $staff->cv }}</td>
			</tr>

			<tr>
				<th width="25%">Photo</th>	
				<td>{{ $staff->photo }}</td>
			</tr>

			<tr>
				<th width="25%">Address</th>	
				<td>{{ $staff->address }}</td>
			</tr>
		</table>
		
	</div>

	<div>

		{{ link_to_route('staff.edit', 'Edit', [$staff->id], ['class'=>'btn btn-info']) }}

		<div class="pull-right">
		{!! Form::open(array(
                              'route'=>[
                              'staff.destroy', $staff->id],
                              'method' => 'DELETE')) !!}
         	  

        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

     
	    {!! Form::close() !!}
		
		</div>
	</div>
	
</div>


@stop