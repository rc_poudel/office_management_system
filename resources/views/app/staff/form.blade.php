<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
        {!! Form::label('fname', 'First Name', ['class' => 'control-label']) !!}
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('first_name')) <p class="help-block validation-color"> * {{ $errors->first('first_name') }}</p> @endif

    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
        {!! Form::label('lname', 'Last Name', ['class' => 'control-label']) !!}
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('last_name')) <p class="help-block validation-color">* {{ $errors->first('last_name') }}</p> @endif
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
        {!! Form::label('position', 'Position', ['class' => 'control-label']) !!}
        <div class="input-group">

            <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            {!! Form::text('position', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('position')) <p class="help-block validation-color">* {{ $errors->first('position') }}</p> @endif

    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
        {!! Form::label('dateofjoin', 'Date Of Join', ['class' => 'control-label']) !!}
        <div class="input-group">
                <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                </span>
            {!! Form::input('date', 'date_of_join', date('Y-m-d'), ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('date_of_join')) <p class="help-block validation-color">* {{ $errors->first('date_of_join') }}</p> @endif

    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 form-padding">
        {!! Form::label('mobile', 'Mobile', ['class' => 'control-label']) !!}
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
            {!! Form::tel('mobile', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('mobile')) <p class="help-block validation-color">* {{ $errors->first('mobile') }}</p> @endif
    </div>

    <div class="col-xs-12 col-sm-12 col-md-8 form-padding">
        {!! Form::label('email', 'E-mail', ['class' => 'control-label']) !!}
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('email')) <p class="help-block validation-color">* {{ $errors->first('email') }}</p> @endif
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 form-padding">
        {!! Form::label('address', ' Address', ['class' => 'control-label']) !!}
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
            {!! Form::text('address', null, ['class' => 'form-control', 'id'=>'textarea']) !!}
        </div>
        @if ($errors->has('address')) <p class="help-block validation-color">* {{ $errors->first('address') }}</p> @endif
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 form-padding">
            <label class="col-xs-12 col-sm-12 col-md-8" for="status">Marital Status</label>
            <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
                <select id="status" class="form-control">
                    <option>Married</option>
                    <option>Unmarried</option>
                </select>
                @if ($errors->has('status')) <p class="help-block validation-color">* {{ $errors->first('status') }}</p> @endif
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
        {!! Form::label('createdat', 'Created At', ['class' => 'control-label']) !!}
        <div class="input-group"><span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                </span>
            {!! Form::datetime('created_at', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('created_at')) <p class="help-block validation-color">* {{ $errors->first('created_at') }}</p> @endif
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
        {!! Form::label('updatedat', 'Updated At', ['class' => 'control-label']) !!}
        <div class="input-group"><span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
            {!! Form::datetime('updated_at', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('updated_at')) <p class="help-block validation-color">* {{ $errors->first('updated_at') }}</p> @endif
    </div>
</div>




