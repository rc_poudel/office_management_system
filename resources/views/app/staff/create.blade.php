@extends('app.layouts.create')
@section('content')

<div class="container">
    <h1>Register staff</h1>
    {{ link_to_route('staff.index', 'View All Staff', null,['class'=>'btn btn-primary']) }}
    <hr>

    @include('app.includes.errors')

    {!! Form::open(['route'=>'staff.store', 'files'=>true]) !!}

    @include('app.staff.form')

    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

    @include('app.staff.upload')

     

</div>

@stop