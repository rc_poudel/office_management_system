@extends('app.layouts.default')
@section('content')

<div class="container">
	<h1>Staff Information<!--  <small><a href="{{ url('/staff/create') }}">Register Staff</a></small> --></h1>
  {{ link_to_route('staff.create', 'Register Staff', null,['class'=>'btn btn-primary']) }}

	<hr>
           <div class="panel panel-default panel-table">
           
            <div class="panel-heading">
              <div class="row">
                	
                  <div class="col col-xs-12 text-right">
                    <!--Search -->
                    {!! Form::open(['url' => '/staff', 'method' => 'GET', 'class' => 'navbar-form navbar-right', 'role' => 'search']) !!}
                        {!! Form::text('search', Request::get('search'), ['class' => 'form-control', 'placeholder'   => Lang::get('Search...')]) !!}
                    {!! Form::close() !!}
                    <!--End search-->
                  </div>
              </div>
            </div>
            <div class="panel-body">
                @include('app.includes.errors')
                <table class="table table-striped table-bordered table-list">
                <thead>
                  <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Position</th>
                    <th class="text-center">Mobile</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Action</th>
                    <th class="text-center">Delete</th>
                  </tr> 
                </thead>
                <tbody>
                @foreach($staff as $s)
                    <tr>
                      <td>{{ $s->first_name." ".$s->last_name }}</td>
                      <td>{{ $s->position }}</td>
                      <td>{{ $s->mobile }}</td>
                      <td>{{ $s->email }}</td>
                      <td align="center">
                        <a class="btn btn-info" href="{{ url('/staff/show', $s->id) }}">View</a>
                        <b>|</b>
                        {{ link_to_route('staff.edit', 'Edit', [$s->id], ['class'=>'btn btn-info']) }}
                      </td>
                      <td align="center">
                          {!! Form::open(array(
                                        'route'=>[
                                                    'staff.destroy', $s->id],
                                        'method' => 'DELETE')) !!}
                          {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                          {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>

              </table>
                <div class="col col-xs-12 text-right">
                    <?= $staff->render() ?>
                </div>
             </div>
          </div>
</div>
@stop