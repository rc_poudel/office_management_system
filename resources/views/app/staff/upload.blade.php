	{!! Form::open(array('url' => '/staff/file/handleUpload', 'files' => true)) !!}

        <!-- Upload Image -->
    	<div class="col-xs-12 col-sm-12 col-md-6 form-padding">
            {!! Form::label('photo', ' Photo', ['class' => 'control-label']) !!}
            {{ Form::file('file', ['class' => 'field']) }}
            {!! Form::token() !!}
            {!! Form::submit('Upload') !!}
        </div>
        
    {!! Form::close() !!}

    {!! Form::open(array('url' => '/staff/file/handleUpload', 'files' => true)) !!}

        <!-- Upload CV -->
        <div class="col-xs-12 col-sm-12 col-md-6 form-padding">
            {!! Form::label('cv', ' CV', ['class' => 'control-label']) !!}
            {{ Form::file('file', ['class' => 'field']) }}
            {!! Form::token() !!}
            {!! Form::submit('Upload') !!}
        </div>

    {!! Form::close() !!}