<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="{{ url('/home') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/staff') }}"><i class="fa fa-users fa-fw"></i> Staff</a>
                        </li>
                        <li>
                            <a href="{{ url('/attendance') }}"><i class="fa fa-sticky-note-o fa-fw"></i> Attendance</a>
                        </li>
                        <li>
                            <a href="{{ url('/salary') }}"><i class="fa fa-money"></i> Salary</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>