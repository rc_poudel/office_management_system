<?php

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['auth']], function () {

	Route::resource('staff', 'StaffController', ['except' => ['show']]);

	Route::get('/staff/show/{id}', 'StaffController@show');

	Route::post('/staff/file/handleUpload', 'StaffController@handleUpload');

    Route::resource('attendance', 'AttendanceController');

    Route::resource('salary', 'SalaryController');

});
