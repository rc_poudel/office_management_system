<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\AttendanceRequest;

use App\Model\Staff;

use App\Model\Attendance;



class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $attendance = Attendance::all();
        return view('app.attendance.attendance', compact('attendance', 'staff'));
    }

    public function create()
    {
        $staff = Staff::all();
        $attendance = Attendance::all();
        return view('app.attendance.create', compact('attendance', 'staff'));
    }

    public function store(AttendanceRequest $request)
    {
        $data = $request->all();

        if ($data['office_time'] == "arrival_time") {
            $data['arrival_time'] = date("Y-m-d h:i:s");
            $data['departure_time'] = date("Y-m-d h:i:s");
            $data['remarks'] = "Present";


        }

        if ($data['office_time'] == "departure_time") {
            $data['departure_time'] = date("Y-m-d h:i:s");

        }

        $attendance = new Attendance;
        $attendance->create($data);
        return redirect('attendance');
    }

}