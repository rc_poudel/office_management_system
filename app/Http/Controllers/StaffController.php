<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StaffRequest;
use Session;
use App\Model\staff;
use \Storage;

class StaffController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $staff = Staff::all();
        $search = \Request::get('search');
        $staff = staff::where('first_name', 'like', '%' . $search . '%')
            ->orderBy('first_name')
            ->paginate(4);
        return view('app.staff.index', compact('staff'));
    }

 
    public function create()
    {
        return view('app/staff/create');
    }

    public function store(StaffRequest $request)
    {
        $data= $request->all();
        $staff = new Staff;
        $staff->create($data);
        return redirect('staff')->with('flash_message', 'Staff Information successfully added!');
    }

    public function show($id)
    {
        $staff = Staff::where('id',$id)->firstOrFail();
        return view('app/staff/show', compact('staff'));
    }

    public function edit($id)
    {
        $staff = Staff::where('id',$id)->firstOrFail();
        return view('app/staff/edit', compact('staff'));
    }


    public function update(Request $request, $id)
    {
         $staff = Staff::findOrFail($id);

            $this->validate($request, [
        'first_name' => 'required',
        'mobile' => 'required'
        ]);

        $data = $request->all();

        $staff->fill($data)->save();

        Session::flash('flash_message', 'Information Update successfully');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $staff = Staff::where('id',$id)->firstOrFail();
        $staff->delete();
        Session::flash('delete_message', 'Successfully deleted!');
        return redirect()->back();     
    }

    public function handleUpload(Request $request){


             if($request->hasFile('file')){
             $file = $request->file('file');
             // $allowedFileTypes = config('app.allowedFileTypes');
             // $maxFileSize = config('app.maxFileSize');
        //     $rules = [
        //         'file' => 'required|mimes:'.$allowedFileTypes.'|max:'.$maxFileSize
        //     ];
        //     $this->validate($request, $rules);
             $fileName = $file->getClientOriginalName();
             $destinationPath = config('app.fileDestinationPath').'/'.$fileName;
             $uploaded = Storage::put($destinationPath, file_get_contents($file->getRealPath()));

             if($uploaded){
        //         UploadedFile::create([
        //             'filename' => $fileName
        //         ]);
             }
         }

         return redirect()->to('/staff/create');
    }



}