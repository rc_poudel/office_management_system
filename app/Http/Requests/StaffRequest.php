<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'first_name'=>'required|min:2|max:16|Alpha',
            'last_name'=>'required|min:3|max:16|Alpha',
            'position'=>'required',
            'mobile'=>'required',
            'email'=>'required|between:3,64|email|unique:users',
            'date_of_join'=>'required|date',
            'address'=>'required|between:3,64',
        ];
    }
}
