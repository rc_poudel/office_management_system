<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table = 'attendances';
    protected $fillable = [
        'staff_id',
        'arrival_time',
        'departure_time',
        'remarks'



    ];
    public function staff(){
        return $this->belongsTo('App\Model\Staff');
    }
}