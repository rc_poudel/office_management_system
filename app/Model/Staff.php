<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Staff extends Model
{
    protected $table = 'Staff';
    protected $fillable = [
    'first_name',
    'last_name',
    'position',
    'mobile',
    'email',
    'date_of_join',
    'address',
    'marital_status',
    'updated_at',
    'created_at'
];
}
